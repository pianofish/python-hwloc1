===========================
Python 3 bindings for hwloc
===========================

This is a fork of python-hwloc, specifically to remain on version 1 of
the hwloc library. It will support only Python 3.

I have retired and am no longer maintaining or developing this. I could work
on it more if anyone is interested in using it.

If there is someone who would like to take it over, please contact me.

Fedora and centOS packages are available on
`COPR <https://copr.fedorainfracloud.org/coprs/streeter/python-hwloc/>`_.

Example build for Debian:

Install `python3-libnuma` from 'https://gitlab.com/guystreeter/python-libnuma', and then

.. code:: bash

   sudo apt-get update
   sudo apt-get install git build-essential libpython3.6-dev \
      libnuma-dev debhelper dh-python python-all python-setuptools python3-all \
        python3-setuptools cython3 libhwloc-dev python3-babel \
        libibverbs1 libibverbs-dev
   git clone https://gitlab.com/guystreeter/python3-hwloc.git
   cd python3-hwloc
   make deb

Then install the ``.deb`` package found in the ``deb_builddir`` folder.
Try it out with ``python3 -c "import hwloc;print(hwloc.version_string())"``
