#
# Copyright 2011-2020 Red Hat, Inc.
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#

PACKAGE=python-hwloc
PYTHON=python
TREE=HEAD

VERSION := $(shell rpm -q --qf '%{VERSION} ' --specfile $(PACKAGE).spec | cut -d' ' -f1)
ifndef VERSION
version=$(shell sed -n '/^Version:\s\+/s/^Version:\s\+\(.[0-9]\)/\1/p' < $(PACKAGE).spec)
VERSION=$(shell /bin/echo $(version))
endif
ifndef VERSION
$(error VERSION must be set!)
endif

RELEASE := $(shell rpmspec -q --qf '%{RELEASE}' --srpm $(PACKAGE).spec | sed -e 's/\.centos$$//' -e 's/\.[^.]*$$//')
RPMDIRS=$(addprefix rpm/, SPECS SOURCES BUILD RPMS SRPMS)
RPM_TARBALL=rpm/SOURCES/$(PACKAGE)-$(VERSION)-$(RELEASE).tar.gz

DEB_BUILDDIR=build_debian
DEB_TARBALL=$(PACKAGE)_$(VERSION).orig.tar.gz
DEB_SRCDIR=$(DEB_BUILDDIR)/$(PACKAGE)-$(VERSION)

setup: chwloc.pyx linuxsched.pyx
	$(PYTHON) ./setup.py build_ext --inplace

$(RPMDIRS):
	mkdir -p $(RPMDIRS) || :

$(RPM_TARBALL): $(RPMDIRS)
	git archive --format=tar --prefix=$(PACKAGE)-$(VERSION)-$(RELEASE)/ $(TREE) | gzip -9 > $@

tarball: $(RPM_TARBALL)

rpm: $(RPM_TARBALL) $(PACKAGE).spec
	rpmbuild -ba --define "_topdir $(PWD)/rpm" --define "_source_filedigest_algorithm 1" --define "_binary_filedigest_algorithm 1" --define "_binary_payload w9.gzdio" --define "_source_payload w9.gzdio" $(PACKAGE).spec

srpm: $(RPM_TARBALL) $(PACKAGE).spec
	rpmbuild -bs --define "_topdir $(PWD)/rpm" --define "_source_filedigest_algorithm 1" --define "_binary_filedigest_algorithm 1" --define "_binary_payload w9.gzdio" --define "_source_payload w9.gzdio" $(PACKAGE).spec


$(DEB_BUILDDIR):
	mkdir -p $@

$(DEB_BUILDDIR)/$(DEB_TARBALL): $(DEB_BUILDDIR)
	git archive --format=tar --prefix=$(PACKAGE)-$(VERSION)/ HEAD | gzip -9 > $(DEB_BUILDDIR)/$(DEB_TARBALL)

$(DEB_SRCDIR): $(DEB_BUILDDIR)/$(DEB_TARBALL)
	cd $(DEB_BUILDDIR); tar -xf $(DEB_TARBALL)

deb-tarball: $(DEB_BUILDDIR)/$(DEB_TARBALL)

deb: $(DEB_SRCDIR) deb-tarball
	cd $(DEB_SRCDIR); dpkg-buildpackage -us -uc


clean:
	rm -rf rpm build linuxsched.c hwloc/*.so chwloc.c doc/guide.pdf doc/.guide.md.html dist python*.egg-info $(DEB_BUILDDIR)
	find . -type f \( -name \*~ -o -name \*.pyc -o -name \*.o \) -delete

git-tag:
	git tag -s $(VERSION)-$(RELEASE)

doc/guide.html: doc/guide.md
	pandoc -o doc/guide.html $<

doc/guide.pdf: doc/guide.md
	pandoc -o doc/guide.pdf -V fontfamily:utopia --toc $<

doc: doc/guide.html doc/guide.pdf
