#
# Copyright (C) 2011-2020 Red Hat, Inc.
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#

%if "0%{?rhel}" == "07"
#
# On the copr EPEL build, python3 is not installed at the time the specfile
# is evaluated. We can't invoke python3 or any of the python3 rpm macros
%global python3_pkgversion 36
%global python3 python%{python3_pkgversion}
%global __python3 /usr/bin/%{python3}
%global python3_sitearch /usr/lib64/python3.6/site-packages

%global licenseDirVer -%{version}
%global _build_doc 0
%else
%global licenseDirVer %{nil}
%global _build_doc 1
%endif

%global debug_package %{nil}
%global _privatelibs chwloc|linuxsched
%global __provides_exclude ^(%{_privatelibs})\\.so

%global _hwloc_version 1.11.9

Name:           python3-hwloc1
Version:        2.3.14
Release:        %{_hwloc_version}%{?dist}
Summary:        Python bindings for hwloc version 1

Group:          Development/Languages
License:        GPLv2+
URL:            https://gitlab.com/guystreeter/python-hwloc1
Source0:        python3-hwloc1-%{version}-%{_hwloc_version}.tar.gz

BuildRequires:  gcc
%if %{_build_doc}
BuildRequires:  pandoc, texlive
%endif
BuildRequires:  %{python3}, %{python3}-devel, %{python3}-setuptools, %{python3}-Cython
BuildRequires:  hwloc-devel >= %{_hwloc_version} libibverbs-devel python3-libnuma

%ifnarch s390 s390x %{arm}
BuildRequires:  numactl-devel
%endif

%description
Python bindings for version 1 of hwloc, The Portable Hardware Locality package

%prep
%setup -q -n python3-hwloc-%{version}-%{_hwloc_version}

%build
%if 0%{?_licensedir:1}
 LICENSEDIR=%{_licensedir}/python3-hwloc%{licenseDirVer}
 export LICENSEDIR
%endif % _licensedir
%{__python3} setup.py build
strip build/lib*/hwloc/*.so
chmod 0755 build/lib*/hwloc/*.so

%install
%if 0%{?_licensedir:1}
 LICENSEDIR=%{_licensedir}/python3-hwloc%{licenseDirVer}
 export LICENSEDIR
%endif % _licensedir
%{__python3} setup.py install --skip-build --root %{buildroot}

%files
%license COPYING
%license LICENSE
%{_docdir}/python3-hwloc
%{python3_sitearch}/hwloc
%{python3_sitearch}/*egg-info

%changelog
* Wed Mar  4 2020 Guy Streeter <guy.streeter@gmail.com> - 2.3.14-1.11.9
- build only Python3 version

* Tue Aug 13 2019 Guy Streeter <guy.streeter@gmail.com> - 2.3.13-1.11.8
- build python3-hwloc not python36-hwloc on epel7

* Sat Aug 10 2019 Guy Streeter <guy.streeter@gmail.com> - 2.3.12-1.11.8
- Fix Python 3 build for CentOS 7

* Tue May  7 2019 Guy Streeter <guy.streeter@gmail.com> - 2.3.11.1-1.11.8
- CentOS 7 build, Python 2 only

* Thu May  2 2019 Guy Streeter <guy.streeter@gmail.com> - 2.3.11-1.11.9
- Buildreq python2-Cython instead of Cython
- Buildreq change for hwloc-devel

* Wed Feb 27 2019 Guy Streeter <guy.streeter@gmail.com> - 2.3.10-1.11.9
- No changes to rpm version. Adding Debian package support

* Tue Oct 30 2018 Guy Streeter <guy.streeter@gmail.com> - 2.3.9-1.11.9
- build-req gcc

* Wed May  9 2018 Guy Streeter <guy.streeter@gmail.com> - 2.3.8-1.11.9
- Fedora 28 version, uses hwloc 1.11.9

* Wed May  9 2018 Guy Streeter <guy.streeter@gmail.com> - 2.3.6-1.11.5
- typos

* Wed Jan  3 2018 Guy Streeter <guy.streeter@gmail.com> - 2.3.5-1.11.5
- final really final pip fixes finally

* Mon Jan  1 2018 Guy Streeter <guy.streeter@gmail.com> - 2.3.4-1.11.5
- setup.py changes to enable pip install
- pip version uses ".0." instead of "-" (2.3.4.0.1.11.5)

* Wed Dec 27 2017 Guy Streeter <guy.streeter@gmail.com> - 2.3.3-1.11.5
- correct calling args to alloc_membind_policy_nodeset and objs_covering_cpuset_by_type

* Wed Nov 22 2017 Guy Streeter <guy.streeter@gmail.com> - 2.3.2-1.11.5
- return None when topology cpuset/nodeset is NULL

* Wed Sep 27 2017 Guy Streeter <guy.streeter@gmail.com> - 2.3.1-1.11.5
- Copyright and email address updates in source code
- Add bool() and len() to the Bitmap class

* Tue Aug 22 2017 Guy Streeter <guy.streeter@gmail.com> - 2.3-1.11.5
- correct implementation of Obj.distances
- correct implementation of distrib()
- add a test for hwloc_distrib()
- correct usage of set_area() in doc/hwloc-hello.py
- correct "next" property in TopologyDiffTooComplex and TopologyDiffObjAttr
- add missing "type" property in TopologyDiffObjAttrString

* Tue Jul 25 2017 Guy Streeter <guy.streeter@gmail.com> - 2.2.2-1.11.5
- deliver documentation
- change git source url

* Fri Jul 21 2017 Guy Streeter <guy.streeter@gmail.com> - 2.2.1-1.11.5
- correct implementation of hwloc_obj.attr.bridge.upstream.pci
- correct hwloc_custom test implementation

* Wed Jul 19 2017 Guy Streeter <guy.streeter@gmail.com> - 2.2-1.11.5
- update for hwloc version 1.11.5, Fedora 26

* Sat Jul  8 2017 Guy Streeter <guy.streeter@gmail.com> - 2.2-1.11.0
- correct implementation of hwloc_obj.attr.bridge.downstream.pci
- Fully implemented the topology diff structs and unions
- Added a programmers guide document

* Mon Sep  5 2016 Guy Streeter <guy.streeter@gmail.com> - 2.1.5-1.11.0
- In the latest Cython, some enums seem to need cdef while others need ctypedef

* Tue Jan  5 2016 Guy Streeter <streeter@redhat.com> - 2.1.4.1-1.11.0
- OBJ_NODE changed to OBJ_NUMANODE
- OBJ_SOCKET changed to _OBJ_PACKAGE

* Wed Dec  2 2015 Guy Streeter <streeter@redhat.com> - 2.1.4-1.11.0
- licensedir macro fix

* Fri Oct  9 2015 Guy Streeter <streeter@redhat.com> - 2.1.3-1.11.0
- add requires python2-libnuma
- work around a libhwloc segfault

* Thu Oct  1 2015 Guy Streeter <streeter@redhat.com> - 2.1.2-1.11.0
- do not return unicode on Python 2
- use the %%py2_build/_install macros

* Tue Sep 15 2015 Guy Streeter <streeter@redhat.com> - 2.1.1-1.11.0
- combine Python 2 and 3
- use hwloc-1.11.0

* Tue Jul 14 2015 Guy Streeter <streeter@redhat.com> - 2.1-1.10.1
- Fedora 22 has hwloc-1.10.1

* Tue Jul  7 2015 Guy Streeter <streeter@redhat.com> - 2.1-1.10.0
- pylint, pyflakes, and other checks

* Wed Jun  3 2015 Guy Streeter <streeter@redhat.com> - 2.0.7-1.10.0
- add bitmap_copy
- allow Bitmap init from other Bitmap
- allow Bitmap() to allocate

* Wed Feb 11 2015 Guy Streeter <streeter@redhat.com> - 2.0.6-1.10.0
- require libibverbs (hwloc should do this)

* Tue Feb 10 2015 Guy Streeter <streeter@redhat.com> - 2.0.5-1.10.0
- support for hwloc-1.10

* Fri Jan 30 2015 Guy Streeter <streeter@redhat.com> - 2.0.5-1.9
- specfile spiffy

* Fri Jan  9 2015 Guy Streeter <streeter@redhat.com> - 2.0.3-1.9
- fix attempts to combine bytes and strings
- return a better exception from sscanf
- present depths as signed values
- fix type_of_string() to allow integers again

* Tue Jan  6 2015 Guy Streeter <streeter@redhat.com> - 2.0.2-1.9
- correctly pass invalid type exception up from type_of_string

* Wed Aug 20 2014 Guy Streeter <streeter@redhat.com> - 2.0.2-1.7
- Python 3 support

* Wed Aug  6 2014 Guy Streeter <streeter@redhat.com> - 2.0.2-1.7
- code and build cleanups

* Thu Apr 24 2014 Guy Streeter <streeter@redhat.com> - 2.0.1-1.7
- version bump
- try to work where libnuma is not available

* Wed Apr 23 2014 Guy Streeter <streeter@redhat.com> - 2.0-1.7.1.6
- depend on python-libnuma to pull in the numa library
- the correct hwloc lib is auto-required

* Tue Apr 08 2014 Guy Streeter <streeter@redhat.com> - 2.0-1.7.1.5
- Fedora 20 has libnuma in a separate rpm

* Tue Apr 01 2014 Guy Streeter <streeter@redhat.com> - 2.0-1.7.1.4
- fix an unsigned conversion in physical index values

* Mon Mar 31 2014 Guy Streeter <streeter@redhat.com> - 2.0-1.7.1.3
- use numactl only on x86_64

* Thu Jan  2 2014 Guy Streeter <streeter@redhat.com> - 2.0-1.7.1.1
- bump the version, since this is a re-write

* Tue Dec 17 2013 Guy Streeter <streeter@redhat.com> - 1.7-1
- hwloc 1.7 support
- conversion to Cython

* Fri Jan  4 2013 Guy Streeter <streeter@redhat.com> - 1.6-0
- hwloc-1.6 support

* Thu Nov 15 2012 Guy Streeter <streeter@redhat.com> - 1.5-2
- Add PCI display to lstopo

* Mon Oct 15 2012 Guy Streeter <streeter@redhat.com> - 1.5-1
- Do not depend on an un-versioned libhwloc.so

* Tue Sep 18 2012 Guy Streeter <streeter@redhat.com> - 1.5-0.1
- initial 1.5 support

* Fri Feb 24 2012 Guy Streeter <streeter@redhat.com> - 1.4-1
- hwloc-1.4 library support
- additional tests and bugfixes

* Wed Nov 16 2011 Guy Streeter <streeter@redhat.com> - 1.3-1
- hwloc-1.3 library support
- additional tests and bugfixes

* Tue Apr 26 2011 Guy Streeter <streeter@redhat.com> - 0.6-1
- hwloc-1.2 library support

* Wed Feb 23 2011 Guy Streeter <streeter@redhat.com> - 0.4-1
- lstopo.py supports some options
- add hwloc-bind.py
- correct handling location specs in calc and bind
- add api_version_string()
- fixed the object infos iterator

* Tue Feb  8 2011 Guy Streeter <streeter@redhat.com> - 0.3-1
- Fedora naming guidelines suggest python-hwloc instead of pyhwloc

* Wed Jan 19 2011 Guy Streeter <streeter@redhat.com> - 0.2
- Continuing development, massive changes

* Wed Nov 24 2010 Guy Streeter <streeter@redhat.com> - 0.1
- Initial build
