#!/usr/bin/env python3
# -*- python -*-

#
# Copyright (C) 2013-2017 Red Hat, Inc.
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#
# This is a re-implementation in python of tests/intel-mic.c from the
# hwloc package.
#

from __future__ import print_function
import hwloc

topology = hwloc.Topology()
topology.set_flags(hwloc.TOPOLOGY_FLAG_IO_DEVICES)
topology.load()

i = 0
for osdev in topology.intel_mic_device_osdevs:

    ancestor = osdev.non_io_ancestor

    print('found OSDEV', osdev.name)
    assert osdev.name.startswith('mic')
    assert int(osdev.name[3:]) == i

    assert osdev.attr.osdev.type == hwloc.OBJ_OSDEV_COPROC

    assert osdev.get_info_by_name('CoProcType') == 'MIC'

    print('found MICFamily', osdev.get_info_by_name('MICFamily'))
    print('found MICSKU', osdev.get_info_by_name('MICSKU'))
    print('found MICActiveCores', osdev.get_info_by_name('MICActiveCores'))
    print('found MICMemorySize', osdev.get_info_by_name('MICMemorySize'))

    try:
        cpuset = topology.intel_mic_get_device_cpuset(i)
    except:
        print('failed to get cpuset for device', i)
    else:
        print('got cpuset', str(cpuset), 'for device', i)

    i += 1
