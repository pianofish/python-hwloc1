#!/usr/bin/env python3
# -*- python -*-

#
# Copyright (C) 2013-2017 Red Hat, Inc.
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#
# This is a re-implementation in python of tests/hwloc_backends.c from the
# hwloc package.
#

from __future__ import print_function
import hwloc
import tempfile

xmlbuf = None
xmlfileok = False

print('trying to export topology to XML buffer and file for later...')
topology1 = hwloc.Topology()

topology1.load()
assert topology1.is_thissystem

try:
    xmlbuf = topology1.export_xmlbuffer()
except OSError as e:
    print('XML buffer export failed (%s), ignoring' % str(e))
with tempfile.NamedTemporaryFile(prefix='hwloc_backends.tmpxml') as xmlfile:
    try:
        topology1.export_xml(xmlfile.name)
        xmlfileok = True
    except OSError as e:
        print('XML file export failed (%s), ignoring' % str(e))

    print('init...')
    topology2 = hwloc.Topology()
    if xmlfileok:
        print('switching to xml...')
        topology2.set_xml(xmlfile.name)
    if xmlbuf:
        print('switching to xmlbuffer...')
        topology2.set_xmlbuffer(xmlbuf)
    print('switching to custom...')
    topology2.set_custom()
    print('switching to synthetic...')
    topology2.set_synthetic('machine:2 node:3 cache:2 pu:4')
    print('switching sysfs fsroot to // ...')
    topology2.set_fsroot('//')  # valid path that won't be recognized as '/'
    print('switching sysfs fsroot to / ...')
    topology2.set_fsroot('/')
    del topology2
    topology2 = hwloc.Topology()

    if xmlfileok:
        print('switching to xml and loading...')
        topology2.set_xml(xmlfile.name)
        topology2.load()
        topology2.check()
        assert not topology2.is_thissystem
        del topology2

topology2 = hwloc.Topology()
if xmlbuf:
    print('switching to xmlbuffer and loading...')
    topology2.set_xmlbuffer(xmlbuf)
    topology2.load()
    topology2.check()
    assert not topology2.is_thissystem
    del topology2
    topology2 = hwloc.Topology()

print('switching to custom and loading...')
topology2.set_custom()
sw = topology2.custom_insert_group_object_by_parent(topology2.root_obj, 0)
assert sw
topology2.custom_insert_topology(sw, topology1)
topology2.load()
topology2.check()
assert not topology2.is_thissystem
del topology2

topology2 = hwloc.Topology()
print('switching to synthetic and loading...')
topology2.set_synthetic('machine:2 node:3 cache:2 pu:4')
topology2.load()
topology2.check()
assert not topology2.is_thissystem
del topology2

topology2 = hwloc.Topology()
print('switching sysfs fsroot to // and loading...')
# '//' isn't recognized as the normal fsroot on Linux, and it fails and falls back to normal topology on !Linux
err = topology2.set_fsroot('//')
topology2.load()
topology2.check()
assert (not topology2.is_thissystem) == (not err)
del topology2

topology2 = hwloc.Topology()
print('switching sysfs fsroot to / and loading...')
#  '/' is recognized as the normal fsroot on Linux, and it fails and falls back to normal topology on !Linux
err = topology2.set_fsroot('/')
topology2.load()
topology2.check()
assert topology2.is_thissystem
del topology2

topology2 = hwloc.Topology()
print('switching to synthetic...')
topology2.set_synthetic('machine:2 node:3 cache:2 pu:4')
del topology2
