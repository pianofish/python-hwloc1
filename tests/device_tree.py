#!/usr/bin/env python3
# -*- python -*-

#
# Copyright (C) 2011-2017 Red Hat, Inc.
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#
# This is an effort to understand how PCI device relationships are
# represented in hwloc.
#

from __future__ import print_function
import hwloc

topo = hwloc.Topology()
topo.set_flags(hwloc.TOPOLOGY_FLAG_WHOLE_IO)
topo.load()

# Let's try finding the bridges, then finding what's below them


class iodev(hwloc.Obj):

    def enumerate(self, indent):
        print('  ' * indent + self.description)
        print('  ' * (indent) + ' ' + self.busid)
        for child in self.children:
            makeType(child)
            child.enumerate(indent + 1)

    @property
    def busid(self):
        return '%04x:%02x:%02x.%01x' % (
            self.attr.pcidev.domain,
            self.attr.pcidev.bus,
            self.attr.pcidev.dev,
            self.attr.pcidev.func)

    @property
    def description(self):
        assert False


class hostBridge(iodev):

    @property
    def description(self):
        return 'host->PCI bridge for domain %04x bus %02x-%02x' % (
            self.attr.bridge.downstream.pci.domain,
            self.attr.bridge.downstream.pci.secondary_bus,
            self.attr.bridge.downstream.pci.subordinate_bus)


class pciBridge(iodev):

    @property
    def description(self):
        return 'PCI->PCI bridge [%04x:%04x] for domain %04x bus %02x-%02x' % (
            self.attr.bridge.upstream.pci.vendor_id,
            self.attr.bridge.upstream.pci.device_id,
            self.attr.bridge.downstream.pci.domain,
            self.attr.bridge.downstream.pci.secondary_bus,
            self.attr.bridge.downstream.pci.subordinate_bus)


class pciDevice(iodev):

    @property
    def description(self):
        return 'PCI device class %04x vendor %04x model %04x' % (
            self.attr.pcidev.class_id,
            self.attr.pcidev.vendor_id,
            self.attr.pcidev.device_id)


class osDevice(iodev):

    @property
    def description(self):
        obj = self
        while not obj.cpuset:
            obj = obj.parent
        return 'OS device %s subtype %d (%s)' % (
            self.name,
            self.attr.osdev.type,
            obj.cpuset)


def makeType(obj):
    if obj.type == hwloc.OBJ_BRIDGE:
        if obj.attr.bridge.upstream_type == hwloc.OBJ_BRIDGE_HOST:
            obj.__class__ = hostBridge
        else:
            assert obj.attr.bridge.upstream_type == hwloc.OBJ_BRIDGE_PCI
            obj.__class__ = pciBridge
    elif obj.type == hwloc.OBJ_PCI_DEVICE:
        obj.__class__ = pciDevice
    elif obj.type == hwloc.OBJ_OS_DEVICE:
        obj.__class__ = osDevice
    else:
        assert False

bridges = list(topo.bridges)

while len(bridges):
    bridge = bridges[0]
    if bridge.attr.bridge.upstream_type != hwloc.OBJ_BRIDGE_HOST:
        bridges.remove(bridge)
        continue
    p = topo.get_non_io_ancestor_obj(bridge)
    print(p.type_string, p.os_index, 'CPUs', p.complete_cpuset)
    if bridge.cpuset is None:
        makeType(bridge)
        bridge.enumerate(1)
        bridges.remove(bridge)
        continue
    for b in topo.objs_inside_cpuset_by_type(p.complete_cpuset, hwloc.OBJ_BRIDGE):
        bridges.remove(b)
        if b.attr.bridge.upstream_type != hwloc.OBJ_BRIDGE_HOST:
            continue
        makeType(b)
        b.enumerate(1)
