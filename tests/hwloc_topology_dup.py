#!/usr/bin/env python3
# -*- python -*-

#
# Copyright (C)2014-2017 Red Hat, Inc.
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#
# This is a re-implementation in python of
# tests/hwloc_topology_dup.c
# from the hwloc package.
#

from __future__ import print_function
import hwloc

node_indexes = list(range(3))
node_distances = [10.0 if i // 3 == i % 3 else 20.0 for i in range(3 * 3)]

core_indexes = list(range(6))
core_distances = [4.0 if i // 6 == i % 6 else 8.0 for i in range(6 * 6)]

oldtopology = hwloc.Topology()
print("building fake 'node:3 core:2 pu:4' topology")
oldtopology.set_synthetic('node:3 core:2 pu:4')
print('adding node and core matrices')
oldtopology.set_distance_matrix(hwloc.OBJ_NODE, node_indexes, node_distances)
oldtopology.set_distance_matrix(hwloc.OBJ_CORE, core_indexes, core_distances)
oldtopology.load()

print('duplicating')
topology = oldtopology.dup()
print('destroying the old topology')
del oldtopology

# remove the entire third node
cpuset = hwloc.Bitmap.alloc()
cpuset.fill()
cpuset.clr_range(16, 23)
topology.restrict(cpuset, hwloc.RESTRICT_FLAG_ADAPT_DISTANCES)
print('checking the result')
assert topology.get_nbobjs_by_type(hwloc.OBJ_NODE) == 2
