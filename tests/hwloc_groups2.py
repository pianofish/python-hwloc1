#!/usr/bin/env python3
# -*- python -*-

#
# Copyright (C) 2011-2017 Red Hat, Inc.
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#
# This is a re-implementation in python of
# tests/hwloc_groups2.c
# from the hwloc package.
#

import hwloc
import os

topo = hwloc.Topology()
topo.set_synthetic('node:2 core:8 pu:1')

# default 2*8*1
topo.load()
depth = topo.depth
assert depth == 4
width = topo.get_nbobjs_by_depth(0)
assert width == 1
width = topo.get_nbobjs_by_depth(1)
assert width == 2
width = topo.get_nbobjs_by_depth(2)
assert width == 16
width = topo.get_nbobjs_by_depth(3)
assert width == 16
del topo

# 2*8*1 and group 8cores as 2*2*2
topo = hwloc.Topology()
topo.set_synthetic('node:2 core:8 pu:1')
indexes = list(range(16))


def values():
    for i in range(16):
        for j in range(16):
            if i == j:
                yield 3
            elif i // 2 == j // 2:
                yield 5
            elif i // 4 == j // 4:
                yield 7
            else:
                yield 9
distances = list(values())
topo.set_distance_matrix(hwloc.OBJ_CORE, indexes, distances)
topo.load()
depth = topo.depth
assert depth == 6
width = topo.get_nbobjs_by_depth(0)
assert width == 1
width = topo.get_nbobjs_by_depth(1)
assert width == 2
width = topo.get_nbobjs_by_depth(2)
assert width == 4
width = topo.get_nbobjs_by_depth(3)
assert width == 8
width = topo.get_nbobjs_by_depth(4)
assert width == 16
del topo

# play with accuracy
distances[0] = 2.9  # diagonal, instead of 3 (0.0333% error)
# smallest group, instead of 5 (0.02% error)
distances[1] = 5.1
distances[16] = 5.2
topo = hwloc.Topology()
topo.set_synthetic('node:2 core:8 pu:1')
topo.set_distance_matrix(hwloc.OBJ_CORE, indexes, distances)
os.environ['HWLOC_GROUPING_ACCURACY'] = '0.1'  # ok
topo.load()
depth = topo.depth
assert depth == 6  # ** 1.2 fails here **
del topo

topo = hwloc.Topology()
topo.set_synthetic('node:2 core:8 pu:1')
topo.set_distance_matrix(hwloc.OBJ_CORE, indexes, distances)
os.environ['HWLOC_GROUPING_ACCURACY'] = 'try'  # ok
topo.load()
depth = topo.depth
assert depth == 6
del topo

topo = hwloc.Topology()
topo.set_synthetic('node:2 core:8 pu:1')
topo.set_distance_matrix(hwloc.OBJ_CORE, indexes, distances)
os.environ['HWLOC_GROUPING_ACCURACY'] = '0.01'  # too small, cannot group
topo.load()
depth = topo.depth
assert depth == 4
del topo

topo = hwloc.Topology()
topo.set_synthetic('node:2 core:8 pu:1')
topo.set_distance_matrix(hwloc.OBJ_CORE, indexes, distances)
os.environ['HWLOC_GROUPING_ACCURACY'] = '0'  # full accuracy, cannot group
topo.load()
depth = topo.depth
assert depth == 4
del topo

# revert to default 2*8*1
topo = hwloc.Topology()
topo.set_synthetic('node:2 core:8 pu:1')
topo.set_distance_matrix(hwloc.OBJ_CORE, indexes, distances)
topo.set_distance_matrix(hwloc.OBJ_CORE, None, None)
topo.load()
depth = topo.depth
assert depth == 4
width = topo.get_nbobjs_by_depth(0)
assert width == 1
width = topo.get_nbobjs_by_depth(1)
assert width == 2
width = topo.get_nbobjs_by_depth(2)
assert width == 16
width = topo.get_nbobjs_by_depth(3)
assert width == 16
del topo

# default 2*4*4
topo = hwloc.Topology()
topo.set_synthetic('node:2 core:4 pu:4')
topo.load()
depth = topo.depth
assert depth == 4
width = topo.get_nbobjs_by_depth(0)
assert width == 1
width = topo.get_nbobjs_by_depth(1)
assert width == 2
width = topo.get_nbobjs_by_depth(2)
assert width == 8
width = topo.get_nbobjs_by_depth(3)
assert width == 32
del topo

# 2*4*4 and group 4cores as 2*2
topo = hwloc.Topology()
topo.set_synthetic('node:2 core:4 pu:4')
os.environ['HWLOC_Core_DISTANCES'] = '0,1,2,3,4,5,6,7:4*2'
topo.load()
depth = topo.depth
assert depth == 5
width = topo.get_nbobjs_by_depth(0)
assert width == 1
width = topo.get_nbobjs_by_depth(1)
assert width == 2
width = topo.get_nbobjs_by_depth(2)
assert width == 4
width = topo.get_nbobjs_by_depth(3)
assert width == 8
width = topo.get_nbobjs_by_depth(4)
assert width == 32
del topo

# 2*4*4 and group 4cores as 2*2 and 4PUs as 2*2
if hwloc.get_api_version() <= 0x00010300:
    # (PU distance with index range works in 1.4 ?)
    s = ''
    for i in range(32):
        s += str(i) + ','
    s = s[:-1] + ':16*2'
else:
    s = '0-31:16*2'
topo = hwloc.Topology()
topo.set_synthetic('node:2 core:4 pu:4')
os.environ['HWLOC_PU_DISTANCES'] = s
topo.load()
depth = topo.depth
assert depth == 6
width = topo.get_nbobjs_by_depth(0)
assert width == 1
width = topo.get_nbobjs_by_depth(1)
assert width == 2
width = topo.get_nbobjs_by_depth(2)
assert width == 4
width = topo.get_nbobjs_by_depth(3)
assert width == 8
width = topo.get_nbobjs_by_depth(4)
assert width == 16
width = topo.get_nbobjs_by_depth(5)
assert width == 32
del topo

# replace previous core distances with useless ones (grouping as the existing numa nodes)
#  2*4*4 and group 4PUs as 2*2
topo = hwloc.Topology()
topo.set_synthetic('node:2 core:4 pu:4')
os.environ['HWLOC_Core_DISTANCES'] = '0,1,2,3,4,5,6,7:2*4'
topo.load()
depth = topo.depth
assert depth == 5
width = topo.get_nbobjs_by_depth(0)
assert width == 1
width = topo.get_nbobjs_by_depth(1)
assert width == 2
width = topo.get_nbobjs_by_depth(2)
assert width == 8
width = topo.get_nbobjs_by_depth(3)
assert width == 16
width = topo.get_nbobjs_by_depth(4)
assert width == 32
del topo

# clear everything
# default 2*4*4
topo = hwloc.Topology()
topo.set_synthetic('node:2 core:4 pu:4')
os.environ['HWLOC_Core_DISTANCES'] = 'none'
os.environ['HWLOC_PU_DISTANCES'] = 'none'
topo.load()
depth = topo.depth
assert depth == 4
width = topo.get_nbobjs_by_depth(0)
assert width == 1
width = topo.get_nbobjs_by_depth(1)
assert width == 2
width = topo.get_nbobjs_by_depth(2)
assert width == 8
width = topo.get_nbobjs_by_depth(3)
assert width == 32
del topo

# buggy tests
topo = hwloc.Topology()
topo.set_synthetic('node:2 core:4 pu:4')
try:
    topo.set_distance_matrix(hwloc.OBJ_CORE, None, None)
    assert False
except:
    pass
try:
    topo.set_distance_matrix(hwloc.OBJ_CORE, indexes, None)
    assert False
except:
    pass
indexes[1] = 0
try:
    topo.set_distance_matrix(hwloc.OBJ_CORE, indexes, distances)
    assert False
except:
    pass
