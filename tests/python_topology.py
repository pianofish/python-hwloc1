#!/usr/bin/env python3
# -*- python -*-

#
# Copyright (C) 2011-2017 Red Hat, Inc.
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#
# This tests the python hwloc_topology implementation
#

from __future__ import print_function
import hwloc
import tempfile

assert hwloc.get_api_version() >= 0x00010100  # earliest supported

# hwloc_topology is the class representing a topology context.
# getting an instance of it does topology_init() automatically
topo = hwloc.Topology()

# you can then (optionally) do any of:
#  ignore_type
#  ignore_type_keep_structure
#  ignore_all_keep_structure
#  set_flags
#  set_fsroot
#  set_pid
#  set_synthetic
#  set_xml
#  set_xmlbuffer
#  get_support

# support can also be accessed as a property
support = topo.support

topo.set_synthetic('machine:3 group:2 group:2 core:3 cache:2 cache:2 2')

# then load
topo.load()

# check? sure
topo.check()

# depth is a property
assert topo.depth == 8

assert topo.get_depth_type(0) == hwloc.OBJ_SYSTEM
assert topo.get_depth_type(7) == hwloc.OBJ_PU

assert topo.get_type_depth(hwloc.OBJ_MACHINE) == 1
assert topo.get_type_depth(hwloc.OBJ_NODE) == hwloc.TYPE_DEPTH_UNKNOWN

assert topo.get_type_or_above_depth(hwloc.OBJ_NODE) == 3
assert topo.get_type_or_below_depth(hwloc.OBJ_NODE) == 4
assert topo.get_type_or_above_depth(
    hwloc.OBJ_CACHE) == hwloc.TYPE_DEPTH_MULTIPLE

assert topo.get_nbobjs_by_depth(0) == 1

assert topo.get_nbobjs_by_type(hwloc.OBJ_PU) == 3 * 2 * 2 * 3 * 2 * 2 * 2

# is_thissytem is a property
assert not topo.is_thissystem

with tempfile.NamedTemporaryFile(prefix='python_topology.tmpxml') as xmlfile:
    topo.export_xml(xmlfile.name)

print(topo.export_xmlbuffer())

# destroy() is done automatically
