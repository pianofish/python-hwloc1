#!/usr/bin/env python3
# -*- python -*-

#
# Copyright (C) 2011-2017 Red Hat, Inc.
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#

from gi import require_version as gi_require_version
gi_require_version('Gtk', '3.0')

import hwloc

from gi.repository import Gtk, Gdk

from math import sqrt


def rectangle(nbobjs, wider=True):
    """Try to arrange a square-ish layout. Very empirical."""
    if nbobjs < 3:
        if wider:
            return nbobjs, 1
        else:
            return 1, nbobjs
    sq = sqrt((nbobjs * 4.0) / 3.0)
    across = int(sq + 0.5)
    sq = sqrt((nbobjs * 3.0) / 4.0)
    down = int(sq + 0.5)
    if across * down < nbobjs:
        across += 1
    trail = nbobjs % across
    if trail != 0 and trail < down:
        across += 1
        down -= 1
    if wider:
        return across, down
    else:
        return down, across


def squareTable(cells, wider=True):
    across, down = rectangle(len(cells), wider)
    table = Gtk.Table(down, across, False)
    i = 0
    for c in cells:
        table.attach(c, i % across, i % across + 1,
                     i // across, i // across + 1)
        i += 1
    return table


class someObj(hwloc.Obj):

    @property
    def description(self):
        return obj_string(self, 1)


class boundingObj(someObj):

    @property
    def description(self):
        return '%s (%s)' % (obj_string(self, 0), self.complete_cpuset)


class iodev(someObj):

    def enumerate(self):
        if self.arity == 0:
            return SomeFrame(self, None)
        cells = []
        for c in self.children:
            makeType(c)
            v = c.enumerate()
            if v:
                cells.append(v)
        if len(cells) == 0:
            return SomeFrame(self, None)
        return SomeFrame(self, squareTable(cells))


class hostBridge(iodev):

    @property
    def description(self):
        return 'host->PCI bridge for domain %04x bus %02x-%02x' % (
            self.attr.bridge.downstream.pci.domain,
            self.attr.bridge.downstream.pci.secondary_bus,
            self.attr.bridge.downstream.pci.subordinate_bus)


class pciBridge(iodev):

    @property
    def description(self):
        return 'PCI->PCI bridge [%04x:%04x] for domain %04x bus %02x-%02x' % (
            self.attr.bridge.upstream.pci.vendor_id,
            self.attr.bridge.upstream.pci.device_id,
            self.attr.bridge.downstream.pci.domain,
            self.attr.bridge.downstream.pci.secondary_bus,
            self.attr.bridge.downstream.pci.subordinate_bus)


class pciDevice(iodev):

    @property
    def description(self):
        return 'PCI device class %04x vendor %04x model %04x' % (
            self.attr.pcidev.class_id,
            self.attr.pcidev.vendor_id,
            self.attr.pcidev.device_id)


class osDevice(iodev):

    @property
    def description(self):
        return 'OS device %s subtype %d' % (
            self.name,
            self.attr.osdev.type)


def makeType(obj, newtype=None):
    if newtype:
        obj.__class__ = newtype
        return
    if obj.type == hwloc.OBJ_BRIDGE:
        if obj.attr.bridge.upstream_type == hwloc.OBJ_BRIDGE_HOST:
            obj.__class__ = hostBridge
        else:
            assert obj.attr.bridge.upstream_type == hwloc.OBJ_BRIDGE_PCI
            obj.__class__ = pciBridge
    elif obj.type == hwloc.OBJ_PCI_DEVICE:
        obj.__class__ = pciDevice
    elif obj.type == hwloc.OBJ_OS_DEVICE:
        obj.__class__ = osDevice
    else:
        obj.__class__ = someObj


def delete_event(_widget, _event, _data=None):
    return False


def destroy(_widget, _data=None):
    Gtk.main_quit()


def keypress(_widget, event):
    if event.keyval in (Gdk.KEY_q, Gdk.KEY_Q, ):
        Gtk.main_quit()
    return True


def obj_string(obj, verbose=0):
    idx = ''
    attr = obj.attr_asprintf(' ', verbose)
    if attr:
        s = '%s%s (%s)' % (obj.type_asprintf(verbose), idx, attr)
    else:
        s = '%s%s' % (obj.type_asprintf(verbose), idx)
    return s


class SomeFrame(Gtk.Frame):

    def __init__(self, obj, child):
        Gtk.Frame.__init__(self)
        self.set_border_width(3)
#        self.modify_bg(gtk.STATE_NORMAL, gtk.gdk.Color(0x00ff, 0x0ff0, 0xff00))
        self.eventbox = Gtk.EventBox()
        self.add(self.eventbox)
        l = Gtk.Label(obj_string(obj))
        l.set_justify(Gtk.Justification.LEFT)
        l.set_alignment(0.0, 0.5)
        l.set_tooltip_text(obj.description)
        if child:
            self.vbox = Gtk.VBox(spacing=5)
            self.vbox.pack_start(l, True, True, 3)
            self.vbox.pack_start(child, True, True, 2)
            self.eventbox.add(self.vbox)
        else:
            self.eventbox.add(l)

    def pack_start(self, widget):
        return self.vbox.pack_start(widget, True, True, 4)

topo = hwloc.Topology()
topo.set_flags(hwloc.TOPOLOGY_FLAG_WHOLE_IO)
topo.load()

bridges = list(topo.bridges)
container = Gtk.VBox(spacing=5)
ancestors = {}
while len(bridges):
    bridge = bridges[0]
    if bridge.attr.bridge.upstream_type != hwloc.OBJ_BRIDGE_HOST:
        bridges.remove(bridge)
        continue
    p = topo.get_non_io_ancestor_obj(bridge)
    makeType(p, boundingObj)
    host_bridges = []
    if bridge.cpuset is None:
        makeType(bridge)
        host_bridges.append(bridge.enumerate())
        bridges.remove(bridge)
    else:
        for b in topo.objs_inside_cpuset_by_type(p.complete_cpuset, hwloc.OBJ_BRIDGE):
            bridges.remove(b)
            # If this isn't a host bridge, we'll find it later
            if b.attr.bridge.upstream_type != hwloc.OBJ_BRIDGE_HOST:
                continue
            # if there is nothing attached here, it is un-interesting
            if b.arity == 0:
                continue
            makeType(b)
            host_bridges.append(b.enumerate())
    f = squareTable(host_bridges, wider=False)
    try:
        a = ancestors[p.os_index * hwloc.OBJ_TYPE_MAX + p.type]
        a.pack_start(f)
    except:
        a = SomeFrame(p, f)
        container.pack_start(a, True, True, 1)
        ancestors[p.os_index * hwloc.OBJ_TYPE_MAX + p.type] = a

window = Gtk.Window()
window.connect('delete_event', delete_event)
window.connect('destroy', destroy)
window.connect('key-press-event', keypress)
window.set_border_width(4)
sc = Gtk.ScrolledWindow()
sc.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)
sc.add_with_viewport(container)
v = container.get_ancestor(Gtk.Viewport)
container.show_all()
window.add(sc)
r = v.size_request()
window.set_default_size(r.width + 20, r.height + 20)
window.show_all()
Gtk.main()
