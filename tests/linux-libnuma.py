#!/usr/bin/env python3
# -*- python -*-

#
# Copyright (C) 2011-2017 Red Hat, Inc.
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#
# This is a re-implementation in python of
# tests/linux-libnuma.c
# from the hwloc package.
#

import hwloc
import libnuma

assert libnuma.Available()

topology = hwloc.Topology()
topology.load()

Set = hwloc.Bitmap.alloc()
nocpunomemnodeset = hwloc.Bitmap.alloc()
nocpubutmemnodeset = hwloc.Bitmap.alloc()
nomembutcpunodeset = hwloc.Bitmap.alloc()
nomembutcpucpuset = hwloc.Bitmap.alloc()

if topology.get_nbobjs_by_type(hwloc.OBJ_NODE):
    for node in topology.objs_by_type(hwloc.OBJ_NODE):
        Set |= node.cpuset
        if node.cpuset.iszero:
            if node.memory.local_memory:
                nocpubutmemnodeset.set(node.os_index)
            else:
                nocpunomemnodeset.set(node.os_index)
        elif not node.memory.local_memory:
            nomembutcpunodeset.set(node.os_index)
            nomembutcpucpuset |= node.cpuset
else:
    Set |= topology.complete_cpuset

set2 = topology.cpuset_from_linux_libnuma_bitmask(libnuma.AllNodes)
# numa_all_nodes_ptr doesn't contain NODES with CPU but no memory
set2 |= nomembutcpucpuset
assert Set == set2

bitmask = topology.cpuset_to_linux_libnuma_bitmask(Set)
# numa_all_nodes_ptr contains NODES with no CPU but with memory
for i in nocpubutmemnodeset:
    bitmask.setbit(i)
assert bitmask == libnuma.AllNodes

bitmask = topology.nodeset_to_linux_libnuma_bitmask(Set)
assert bitmask == libnuma.AllNodes

# convert empty stuff between cpuset and libnuma
bitmask = libnuma.BitmaskAlloc(1)
Set = topology.cpuset_from_linux_libnuma_bitmask(bitmask)
assert Set.iszero

#bitmask = libnuma.BitmaskAlloc(1)
#mask =  topology.cpuset_to_linux_libnuma_ulongs(bitmask)
maxnode = libnuma.MaxNode()

#Set = topology.cpuset_from_linux_libnuma_ulongs(mask)
#assert Set.iszero

# try:
#    #this should fail if there are any NUMA nodes
#    Set = topology.cpuset_from_linux_libnuma_ulongs(mask, maxnode+1)
#    if libnuma.MaxNode():
#        assert 0
# except hwloc.ArgError:
#    pass

Set = hwloc.Bitmap.alloc()
bitmask = topology.cpuset_to_linux_libnuma_bitmask(Set)
bitmask2 = libnuma.BitmaskAlloc(1)
assert bitmask == bitmask2

#Set = hwloc.Bitmap.alloc()
#mask = topology.cpuset_to_linux_libnuma_ulongs(Set)
#assert mask is None or type(mask) is type(libnuma.UlongArray(1))

# convert empty stuff between nodeset and libnuma
bitmask = libnuma.BitmaskAlloc(1)
Set = topology.nodeset_from_linux_libnuma_bitmask(bitmask)
assert Set.iszero

#mask = libnuma.UlongArray(1)
#Set = topology.nodeset_from_linux_libnuma_ulongs(mask)

# try:
# this sould fail if there are any NUMA nodes
#    Set = topology.nodeset_from_linux_libnuma_ulongs(mask,  maxnode+1)
#    if libnuma.MaxNode():
#        assert 0
# except hwloc.ArgError:
#    pass

Set = hwloc.Bitmap.alloc()
bitmask = topology.nodeset_to_linux_libnuma_bitmask(Set)
bitmask2 = libnuma.BitmaskAlloc(1)
assert bitmask == bitmask2

#Set = hwloc.Bitmap.alloc()
#mask = topology.nodeset_to_linux_libnuma_ulongs(Set)
#assert mask is None or type(mask) is type(libnuma.UlongArray(libnuma.MaxNode()))

# convert first node (with CPU and memory) between cpuset/nodeset and libnuma
node = topology.get_next_obj_by_type(hwloc.OBJ_NODE)
while node and (not node.memory.local_memory or node.cpuset.iszero):
    node = node.sibling
if node:
    # convert first node between cpuset and libnuma
    bitmask = topology.cpuset_to_linux_libnuma_bitmask(node.cpuset)
    assert bitmask.isbitset(node.os_index)
    bitmask.clearbit(node.os_index)
    bitmask2 = libnuma.BitmaskAlloc(node.os_index + 1)
    assert bitmask == bitmask2

    mask = topology.cpuset_to_linux_libnuma_ulongs(node.cpuset)
    assert mask is None or isinstance(mask, tuple)
    if node.os_index >= maxnode:
        assert maxnode == node.os_index + 1
        assert mask[0] == 0
    else:
        assert maxnode == node.os_index + 1
        assert mask[0] == 1 << node.os_index

    bitmask = libnuma.BitmaskAlloc(node.os_index + 1)
    bitmask.setbit(node.os_index)
    Set = topology.cpuset_from_linux_libnuma_bitmask(bitmask)
    assert Set == node.cpuset

    mask = []
    if node.os_index < maxnode:
        mask[0] = 1 << node.os_index
    Set = topology.cpuset_from_linux_libnuma_ulongs(mask)
    assert Set == node.cpuset

    # convert first node between nodeset and libnuma
    bitmask = topology.nodeset_to_linux_libnuma_bitmask(node.nodeset)
    assert bitmask.isbitset(node.os_index)
    bitmask.clearbit(node.os_index)
    bitmask2 = libnuma.BitmaskAlloc(node.os_index + 1)
    assert bitmask == bitmask2

    mask = topology.nodeset_to_linux_libnuma_ulongs(node.nodeset)
    assert mask is None or isinstance(mask, tuple)
    if node.os_index >= maxnode:
        assert mask[0] == 0
    else:
        assert mask[0] == 1 << node.os_index

    bitmask = libnuma.BitmaskAlloc(node.os_index + 1)
    bitmask.setbit(node.os_index)
    Set = topology.nodeset_from_linux_libnuma_bitmask(bitmask)
    assert Set == node.nodeset

    mask = []
    if node.os_index < maxnode:
        mask[0] = 1 << node.os_index
    Set = topology.nodeset_from_linux_libnuma_ulongs(mask)
    assert Set == node.nodeset
